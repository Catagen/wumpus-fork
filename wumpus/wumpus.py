import sys
from math import sqrt
import pygame
from pygame.locals import *

from const import *
from map import Dungeon

try:
    from agent import Agent
except ImportError:
    raise Exception("Failed to import Agent!")

SIZE = 16
SQUARE_SIZE = 24

S_WAIT = 1
S_SIMULATE = 2
S_MULTISIM = 3
S_RESULTS = 4
S_NEWMAP = 5

class GameController():
    def __init__(self):
        self.state = S_WAIT
        self.tick = 0
        self.real_tick = 0
        self.agent = Agent()
        self.clock = pygame.time.Clock()

        ## Init pygame
        pygame.init()
        self.screen = pygame.display.set_mode((SIZE * SQUARE_SIZE, SIZE * SQUARE_SIZE + 64), 0, 32)
        pygame.display.set_caption('Hunt the Wumpus')
        self.font = pygame.font.SysFont('monospace', 14)
        self.font_large = pygame.font.SysFont('monospace', 36)

        self.map_options = {'pits': 10, 'square': False, 'irregular_size': 100}
        self.map = Dungeon(self.screen, self.font, SIZE, SQUARE_SIZE, **self.map_options)

    def run(self):
        while True:
            if self.state == S_WAIT:
                for event in pygame.event.get():
                    if event.type == QUIT:
                        pygame.quit()
                        sys.exit()
                    if event.type == KEYDOWN:
                        if event.key == K_ESCAPE:
                            pygame.quit()
                            sys.exit()
                        elif event.key == K_SPACE:
                            self.state = S_SIMULATE
                        elif event.key == K_x:
                            self.stats = {'wins': 0, 'deaths': 0, 'timeouts': 0, 'scores': [], 'wumpus_kills': 0, 'gold_found': 0}
                            self.state = S_MULTISIM
                        elif event.key == K_q:
                            self.map_options['pits'] = self.map_options['pits'] + 5 if self.map_options['pits'] < 50 else 50
                        elif event.key == K_a:
                            self.map_options['pits'] = self.map_options['pits'] - 5 if self.map_options['pits'] > 5 else 5
                        elif event.key == K_z:
                            self.map_options['square'] ^= True
                        elif event.key == K_n:
                            self.map = Dungeon(self.screen, self.font, SIZE, SQUARE_SIZE, **self.map_options)
                        elif event.key == K_w:
                            self.map_options['irregular_size'] = self.map_options['irregular_size'] + 25 if self.map_options['irregular_size'] < 175 else 175
                        elif event.key == K_s:
                            self.map_options['irregular_size'] = self.map_options['irregular_size'] - 25 if self.map_options['irregular_size'] > 25 else 25

                self.draw()
                self.map.draw()

            if self.state == S_NEWMAP:
                opts = self.map_options
                self.map = Dungeon(self.screen, self.font, SIZE, SQUARE_SIZE, **opts)

                self.state == S_WAIT

            if self.state == S_MULTISIM:
                for i in range(100):
                    try:
                        map = Dungeon(self.screen, self.font, SIZE, SQUARE_SIZE)
                        agent = Agent()
                        num_steps = 0
                        while map.agent_alive and not map.agent_exited and num_steps < 1000:
                            stimuli = map.get_stimuli()
                            action = agent.step(*stimuli)
                            map.make_move(action)
                            num_steps += 1

                        if num_steps >= 1000:
                            self.stats['scores'].append(map.points + 1000)
                            self.stats['timeouts'] += 1
                        else:
                            self.stats['scores'].append(map.points)

                        if map.agent_alive and map.agent_exited:
                            #print "Agent lived!"
                            self.stats['wins'] += 1

                        if not map.agent_alive:
                            #print "Agent died. :("
                            self.stats['deaths'] += 1

                        if not map.wumpus_alive:
                            self.stats['wumpus_kills'] += 1

                        if map.gold_found:
                            self.stats['gold_found'] += 1

                        #print i
                        self.screen.fill((50, 50, 50))
                        text = self.font_large.render("{}".format(i + 1), 1, (255, 255, 255))
                        self.screen.blit(text, (SQUARE_SIZE * SIZE / 2 - text.get_width() / 2, SQUARE_SIZE * SIZE / 2))
                        pygame.display.flip()
                    except:
                        pass

                self.stats['average'] = sum(self.stats['scores']) / len(self.stats['scores'])
                self.stats['variance'] = sum([(x - self.stats['average']) ** 2 for x in self.stats['scores']]) / (len(self.stats['scores']) - 1)
                self.stats['std_dev'] = sqrt(self.stats['variance'])

                self.stats['max'] = max(self.stats['scores'])
                self.stats['min'] = min(self.stats['scores'])

                self.state = S_RESULTS

            if self.state == S_RESULTS:
                for event in pygame.event.get():
                    if event.type == QUIT:
                        pygame.quit()
                        sys.exit()
                    if event.type == KEYDOWN:
                        if event.key == K_ESCAPE:
                            pygame.quit()
                            sys.exit()

                self.draw_result()


            if self.state == S_SIMULATE:
                for event in pygame.event.get():
                    if event.type == QUIT:
                        pygame.quit()
                        sys.exit()
                    if event.type == KEYDOWN:
                        if event.key == K_ESCAPE:
                            pygame.quit()
                            sys.exit()
    
                if self.tick % 5 == 0:
                    self.tick = 0
                    self.real_tick += 1
                    if self.map.agent_alive and not self.map.agent_exited:
                        print '-- @ tick {0:>4} {1}'.format(self.real_tick, '-' * 63)
                        stimuli = self.map.get_stimuli()
                        action = self.agent.step(*stimuli)
                        self.map.make_move(action)
                        s = []
                        if stimuli[0]:
                            s.append('GLITTER')
                        if stimuli[1]:
                            s.append('STENCH')
                        if stimuli[2]:
                            s.append('BREEZE')
                        if stimuli[3]:
                            s.append('BUMP')
                        if stimuli[4]:
                            s.append('SCREAM')

                        a = ('WALK', 'ROTATE_LEFT', 'ROTATE_RIGHT', 'FIRE', 'CLIMB', 'LOOT')[action - 1]
                        print '  {} => {}'.format(', '.join(s), a)
                        print '  Now facing {}'.format(('EAST', 'SOUTH', 'WEST', 'NORTH')[self.map.agent_direction])

                        if not self.map.agent_alive:
                            print '\n  ==> Player died! <=='

                        if self.map.agent_exited:
                            print '\n  ==> Player exited the map. <=='

                self.draw()
                self.map.draw()


            pygame.display.flip()
            self.clock.tick(30)
            self.tick += 1

    def draw(self):
        self.screen.fill((50, 50, 50))

        text = self.font.render("LOADED AGENT: {}".format(self.agent), 1, (255, 255, 255))
        self.screen.blit(text, (10, SIZE * SQUARE_SIZE + 8))
        
        text = self.font.render("POINTS: {}".format(self.map.points), 1, (255, 255, 255))
        self.screen.blit(text, (10, SIZE * SQUARE_SIZE + 24))

        text = self.font.render("MOVES: {}".format(self.map.total_moves), 1, (255, 255, 255))
        self.screen.blit(text, (10, SIZE * SQUARE_SIZE + 40))

        text = self.font.render("# PITS: {}".format(self.map_options['pits']), 1, (255, 255, 255))
        self.screen.blit(text, (280, SIZE * SQUARE_SIZE + 8))

        text = self.font.render("SQUARE: {}".format('Yes' if self.map_options['square'] else 'No'), 1, (255, 255, 255))
        self.screen.blit(text, (280, SIZE * SQUARE_SIZE + 24))

        text = self.font.render("SIZE: {}".format(self.map_options['irregular_size'] if not self.map_options['square'] else 'N/A'), 1, (255, 255, 255))
        self.screen.blit(text, (280, SIZE * SQUARE_SIZE + 40))


    def draw_result(self):
        self.screen.fill((50, 50, 50))

        text = self.font.render("Wins: {}".format(self.stats['wins']), 1, (255, 255, 255))
        self.screen.blit(text, (10, 10))

        text = self.font.render("Losses: {}".format(self.stats['deaths']), 1, (255, 255, 255))
        self.screen.blit(text, (10, 40))

        text = self.font.render("Timeouts: {}".format(self.stats['timeouts']), 1, (255, 255, 255))
        self.screen.blit(text, (10, 70))
        
        text = self.font.render("Lowest score: {:d}".format(self.stats['min']), 1, (255, 255, 255))
        self.screen.blit(text, (10, 100))

        text = self.font.render("Highest score: {:d}".format(self.stats['max']), 1, (255, 255, 255))
        self.screen.blit(text, (10, 130))

        text = self.font.render("Average: {:d}".format(self.stats['average']), 1, (255, 255, 255))
        self.screen.blit(text, (10, 160))

#        text = self.font.render("Variance: {:d}".format(self.stats['variance']), 1, (255, 255, 255))
#        self.screen.blit(text, (10, 130))

        text = self.font.render("Standard deviation: {:.2f}".format(self.stats['std_dev']), 1, (255, 255, 255))
        self.screen.blit(text, (10, 190))

        text = self.font.render("Wumpus kills: {:d}".format(self.stats['wumpus_kills']), 1, (255, 255, 255))
        self.screen.blit(text, (10, 220))

        text = self.font.render("Gold found: {:d}".format(self.stats['gold_found']), 1, (255, 255, 255))
        self.screen.blit(text, (10, 250))
