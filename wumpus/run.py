"""Hunt the Wumpus

Replace the agent.py file with one containing your own agent. Make sure the 
class Agent exists and has a proper step method.

SPACE: Start a single simulation with the map currently presented on screen.
N:     Generate a new map with the settings displayed.
Q/A:   Change amount of pits in generated map.
Z:     Switch from irregular to square shaped maps.
W/S:   Adjust the map size for irregular shaped maps.

"""

from wumpus import GameController

game = GameController()
game.run()